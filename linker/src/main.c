////////////////////////////////////////////////////////////////////////////////
///
/// File:       main.c
/// Date:       2017-06-10
///
/// Authors:    Antonio Cortes Rodrigues
///             Marcelo Luiz Harry Diniz Lemos
///             Pedro Henrique Martins Brito Aguiar
///
/// Description:
///     [file description]
///
////////////////////////////////////////////////////////////////////////////////

////////////////////////////// INCLUDED LIBRARIES //////////////////////////////

#include <stdio.h>
#include <stdlib.h>

#include "linker.h"

///////////////////////////////////// MAIN /////////////////////////////////////

int main(int argc, char const *argv[]) {

    return Link(argc, argv);
}
