################################################################################
###
###	File: 		makefile
###	Date:		2017-05-04
###
### Authors:	Antonio Cortes Rodrigues
###				Marcelo Luiz Harry Diniz Lemos
###				Pedro Henrique Martins Brito Aguiar
###
###	Description:
###		[file description]
###
################################################################################

################################## VARIABLES ###################################

# Project name
PROJ = mont

# Executable name
EXEC = $(PROJ)

# Documentation
DOC = Documentation.pdf

# Compiler and flags
CC = gcc
CFLAGS = -g -Wall -Wextra -Werror -std=c99 -pedantic

# Source and object directories
SRCDIR = src
OBJDIR = obj

# Sources, includes, and objects
SRC := $(wildcard $(SRCDIR)/*.c)
DEP := $(wildcard $(SRCDIR)/*.h)
OBJ := $(patsubst %.c,$(OBJDIR)/%.o,$(notdir $(SRC)))

# Valgrind flags
VFLAGS = --leak-check=full --leak-resolution=high --show-reachable=yes \
		 --track-origins=yes --error-exitcode=1

# Input directory
INPDIR = ../tst/teste

# Output directory
OUTDIR = out

# Input for testing
INPUT = main.a

# Output for testing
OUTPUT = main.o

# Test execution command
TEST = ./$(EXEC) $(INPDIR)/$(INPUT) $(OUTDIR)/$(OUTPUT)

# Packages needed for all functionalities
PAK = valgrind texlive texlive-latex-extra texlive-lang-portuguese \
	  texlive-science

#################################### RULES #####################################

# Rules for compiling
all: $(EXEC)

$(EXEC): $(OBJDIR) $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(DEP)
	$(CC) $(CFLAGS) -c $< -o $@

# Rule for creating directories
$(OBJDIR) $(OUTDIR):
	@mkdir -p $@

# Rules for testing
run: $(EXEC) $(OUTDIR)
	$(TEST)

valgrind: $(EXEC) $(OUTDIR)
	valgrind $(VFLAGS) $(TEST)

# Rule for compiling the doc
$(DOC): FORCE
	@cd ../doc && $(MAKE) --no-print-directory && mv $(DOC) ..

# Rule that zips the project
# $(PROJ).zip: $(DOC)
# 	@mkdir -p zip/$(PROJ)
# 	@cp -ar makefile src $(DOC) zip/$(PROJ)
# 	@cd zip && zip -r $(PROJ).zip $(PROJ) && mv $@ ..
# 	@rm -rf zip

# Rule for cleaning the directory
clean:
	rm -rf $(EXEC) $(OBJDIR) $(GEN) $(DOC) $(PROJ).zip

# Rule for installing needed packages
install:
	sudo apt-get install $(PAK)

.PHONY: all run valgrind toys clean install $(TOYS)

FORCE:
