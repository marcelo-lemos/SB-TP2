////////////////////////////////////////////////////////////////////////////////
///
/// File:       linker.c
/// Date:       2017-06-10
///
/// Authors:    Antonio Cortes Rodrigues
///             Marcelo Luiz Harry Diniz Lemos
///             Pedro Henrique Martins Brito Aguiar
///
/// Description:
///     [file description]
///
////////////////////////////////////////////////////////////////////////////////

////////////////////////////// INCLUDED LIBRARIES //////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "linker.h"

////////////////////////////////// FUNCTIONS ///////////////////////////////////

void InitializeOutput(FILE * output) {
    fprintf(output, "WIDTH = %d;\nDEPTH = %d;\n", WIDTH, DEPTH);
    fprintf(output, "ADDRESS_RADIX = BIN;\nDATA_RADIX = BIN;\nCONTENT\nBEGIN\n");
}

void FinalizeOutput(FILE * output) {
    fprintf(output, "END;\n");
}

void PrintLookupTable(LabelType *lookupTable, int totalLabel, FILE * stream) {
    int i;

    for(i = 0; i < totalLabel; i++) {
        fprintf(stream, "%s %d\n", lookupTable[i].label, lookupTable[i].address);
    }
    fprintf(stream, "TABLE END\n");
}

void CarryIn(char * bin, int i) {
    if(i < 0)
        return;
    if(bin[i] == '0') {
        bin[i] = '1';
        return;
    } else {
        bin[i] = '0';
        CarryIn(bin, i - 1);
    }
}

void SignedBinary(char * bin) {
    int i;

    for(i = 0; i < 8; i++) {
        if(bin[i] == '0')
            bin[i] = '1';
        else
            bin[i] = '0';
    }
    CarryIn(bin, 7);
}

void DecimalToBinary(int number, char * bin) {
    int i;
    int isNegative;

    if(number < 0) {
        isNegative = 1;
        number *= -1;
    } else {
        isNegative = 0;
    }
    memset(bin, 0, 9);
    bin[8] = '\0';
    for(i = 7; i >= 0 ; i--) {
        bin[i] = (number % 2) + '0';
        number /= 2;
    }
    if(isNegative)
        SignedBinary(bin);
}

int GetAddress(char * label, LabelType * lookupTable, int totalLabel) {
    int i;

    for(i = 0; i < totalLabel; i++) {

        if(!strcmp(lookupTable[i].label, label))
            return lookupTable[i].address;
    }
    printf("Label not found %s\n", label);
    return -1;
}

int Link(int argc, char const *argv[]) {
    LabelType lookupTable[MAX_NUMBER_LABEL];
    FILE * input;
    FILE * output;
    FILE * aux;
    int i;
    int totalSize;
    char buffer[MAX_LINE_SIZE];
    char currentLabel[MAX_LABEL_SIZE];
    int address;
    int totalLabel;
    int currentPosition;
    char binPos[9];
    char addr[9];
    char label[MAX_LABEL_SIZE];

    aux = tmpfile();
    totalSize = 0;
    totalLabel = 0;
    for(i = 2; i < argc; i++) {
        input = fopen(argv[i], "rt");
        if(input == NULL) {
            printf("Error opening input file!");
            return(-1);
        }
        fscanf(input, "%[^\n]\n", buffer);
        while(strcmp(buffer, "TABLE END")) {
            sscanf(buffer, "%s %d", currentLabel, &address);
            strcpy(lookupTable[totalLabel].label, currentLabel);
            lookupTable[totalLabel].address = address + totalSize;
            totalLabel++;
            fscanf(input, "%[^\n]\n", buffer);
        }
        fscanf(input, "%[^\n]\n", buffer);
        totalSize += atoi(buffer);
        while(!feof(input)) {
            fscanf(input, "%[^\n]\n", buffer);
            fprintf(aux, "%s\n", buffer);
        }
        fclose(input);
    }
    // PrintLookupTable(lookupTable, totalLabel, stdout);
    output = fopen(argv[1], "wt");
    if(output == NULL) {
        printf("Error opening output file!");
        return(-1);
    }
    InitializeOutput(output);
    fseek(aux, SEEK_SET, 0);
    currentPosition = 0;
    while(!feof(aux)) {
        fscanf(aux, "%[^\n]\n", buffer);
        DecimalToBinary(currentPosition, binPos);
        fprintf(output, "%s : ", binPos);
        sscanf(buffer,"%s", label);
        if(label[0] == '_') {
            DecimalToBinary(GetAddress(label,lookupTable, totalLabel), addr);
            fprintf(output, "%s;\n", addr);
        } else {
            fprintf(output, "%s\n", buffer);
        }
        currentPosition++;
    }
    if(currentPosition < 255) {
        DecimalToBinary(currentPosition, binPos);
        fprintf(output, "[%s..11111111] : 00000000;\n", binPos);
    } else if(currentPosition == 255) {
        fprintf(output, "11111111 : 00000000;\n");
    }
    FinalizeOutput(output);
    fclose(output);
    fclose(aux);
    return 0;
}
